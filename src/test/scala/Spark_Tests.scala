package com.surahman.scala.twitter.analytics

import java.io.ByteArrayOutputStream

import org.apache.spark.rdd.RDD
import org.apache.spark.streaming.{StreamingContext, StreamingContextState}
import org.scalatest.BeforeAndAfterEach
import org.scalatest.funsuite.AnyFunSuite

class Spark_Tests
    extends AnyFunSuite
        with BeforeAndAfterEach
{
    //-- Data Member(s)
    private var ssc: StreamingContext = null
    private var testApp: TwitterAnalytics = null

    // <------ [ SETUP ] ------>
    override def beforeEach()
    {
        val args: Array[String] = Array("consumer-key", "consumer-secret", "access-token", "access-token-secret",
                                        "checkptDir", "7")
        testApp = new TwitterAnalytics(args=args)
        ssc = testApp.init()
    }
    // <------ [ SETUP ] ------>


    test("<init> is set to local master.")
    {
        assert(ssc.sparkContext.isLocal, "Spark Context was not setup locally.")
    }

    test("<init> has app name correctly set.")
    {
        assert(ssc.sparkContext.appName === "Twitter Analytics -- Local", "Spark Context was not setup locally.")
    }

    test("<init> Streaming Context has been initialized.")
    {
        assert(ssc.getState() == StreamingContextState.INITIALIZED, "Failed to initialize stream.")
    }

    test("<setupTwitterFeed> has initialized the <Twitter> stream")
    {
        assert(testApp.getTwitterStreamStatus, "<Twitter> stream not setup.")
    }

    test("<displayRDD> display populated RDD with delta information to stdout.")
    {
        val rdd: RDD[(String, Int)] = ssc.sparkContext.parallelize(Array(("one", 1), ("two", 2), ("three", 3)))
        val previous = 18d
        val expected: Double = ((rdd.count - previous) / previous) * 100
        val label = "POPULATED RDD TEST"

        assertResult(expected,"Incorrect delta value returned.")
        { testApp.displayRDD(label, previous, rdd.count) }

    }

    test("<displayRDD> display empty RDD with delta information to stdout.")
    {
        val rdd: RDD[(String, Int)] = ssc.sparkContext.emptyRDD
        val previous = 33d
        val expected: Double = ((rdd.count - previous) / previous) * 100
        val label = "EMPTY RDD TEST"

        assertResult(expected,"Incorrect delta value returned.")
        { testApp.displayRDD(label, previous, rdd.count) }

    }

    test("<displayRDD> display populated RDD with zero previous and delta information to stdout.")
    {
        val rdd: RDD[(String, Int)] = ssc.sparkContext.parallelize(Array(("one", 1), ("two", 2), ("three", 3)))
        val previous = 0d
        val expected: Double = ((rdd.count - 1) / 1) * 100
        val label = "POPULATED RDD TEST"

        assertResult(expected,"Incorrect delta value returned.")
        { testApp.displayRDD(label, previous, rdd.count) }

    }

    test("<displayBanner> test for output.")
    {
        val expected: String = """|
                                  |------------------------------------------------
                                  |               TWITTER ANALYTICS
                                  |Window Duration: 60sec   Slide Duration: 10sec
                                  |------------------------------------------------
                                  |""".stripMargin
        val actual = new ByteArrayOutputStream()
        Console.withOut(actual)
        { testApp.displayBanner() }
        assert(actual.toString.equals(expected), "Failed due to mismatch in expected output.")
    }


    // <------ [ TEARDOWN ] ------>
    override def afterEach()
    {
        if (ssc != null) { ssc.stop() }
    }
    // <------ [ TEARDOWN ] ------>
}
