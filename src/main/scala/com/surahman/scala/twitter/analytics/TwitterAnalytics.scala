package com.surahman.scala.twitter.analytics

            /*__________________________________________________#
            #  Life is a journey, and it's the experiences we   #
            #  have along the way that define us. To shy from   #
            #  the difficult path least traveled is a travesty, #
            #  for it's here that we find the greatest bounty.  #
            #                                                   #
            #               ~ Perpetual Form ~                  #
            #                                                   #
            #   For my father, who taught me the true meaning   #
            #                   of courage.                     #
            #___________________________________________________#
            # File Name    :    TwitterAnalytics.scala          #
            # Dependencies :    LIB:                            #
            #                   USR:                            #
            # Toolchain    :    Scala 2.12.12, SBT 1.4.1,       #
            #                   Spark 3.0.1                     #
            # Compilation  :    sbt assembly                    #
            #___________________________________________________#
            #    https://www.linkedin.com/in/saad-ur-rahman/    #
            #  Copyright Saad Ur Rahman, All rights reserved.   #
            #     Usage and source code is subject to the       #
            #           GNU Affero GPL v3.0 license.            #
            #___________________________________________________#
            #                                                   #
            #            Twitter Analytics Utilities            #
            #__________________________________________________*/




//- Import(s): Library:
import org.apache.log4j.{Level, Logger}
import org.apache.spark.SparkConf
import org.apache.spark.streaming.dstream.{DStream, ReceiverInputDStream}
import org.apache.spark.streaming.twitter.TwitterUtils
import org.apache.spark.streaming.{Duration, Seconds, StreamingContext}
import twitter4j.Status

import scala.util.matching.Regex



            /*_________________________________________________ #
            #               Object Definitions                  #
            #__________________________________________________*/


/**
 * Runs analytics on a live stream from Twitter collecting key dimensional data.
 * @param k_WindowDuration Total duration of time into the past to look at. @default: 60sec
 * @param k_SlideDuration Total duration of time slide data window forward by. @default: 10sec
 * @param k_BatchDuration Duration of the batch passed to the sliding window.
 * @param args Argument container with <Twitter> credentials, checkpoint directory, and threshold.
 */
class TwitterAnalytics
(
    //-- Data Member(s):
    val k_WindowDuration: Duration = Seconds(60),
    val k_SlideDuration: Duration = Seconds(10),
    val k_BatchDuration: Duration = Seconds(2),
    val args: Array[String]
) extends Serializable
{
    //-- Data Member(s):
    private var m_Threshold: Int = 5
    private var m_CheckpointDir: String = "twitter_analytics_checkpt"
    private var m_TweetStream: ReceiverInputDStream[Status] = null
    private val k_Appname: String = "Twitter Analytics"


    // <<----        BEGIN: CONSTRUCTOR        ---->>

    // Safety Check(s):
    if (args.length < 4 || args.length > 6)
    {
        throw new IllegalArgumentException("[ ERROR: Incorrect arguments supplied on cmdl: " +
                                           "<consumer-key consumer-secret access-token " +
                                           "access-token-secret checkpoint-directory [threshold]> ]")
    }

    // Extract mandatory parameters.
    val Array(consKey, consSecret, accToken, accTokenSecret) = args.take(4)

    // Setup <m_CheckpointDir>.
    try ( m_CheckpointDir = args(4) )
    catch { case error: Exception => System.err.println("[ WARN: Using default <checkpoint> directory ]") }

    // Setup <m_Threshold> value.
    try ( m_Threshold = args(5).toInt )
    catch { case error: Exception => System.err.println("[ WARN: Using default <threshold>. ]") }


    // <<---- BEGIN: CONFIGURE TWITTER CREDENTIALS ---->>
    System.setProperty("twitter4j.oauth.consumerKey", consKey)
    System.setProperty("twitter4j.oauth.consumerSecret", consSecret)
    System.setProperty("twitter4j.oauth.accessToken", accToken)
    System.setProperty("twitter4j.oauth.accessTokenSecret", accTokenSecret)
    // <<---- END: CONFIGURE TWITTER CREDENTIALS ---->>

    // <<----         END: CONSTRUCTOR         ---->>


    /**
     * Getter for the threshold value.
     * @return String The set threshold value.
     */
    def getThreshold: Int = { m_Threshold }


    /**
     * Getter for the checkpoint directory.
     * @return String The set checkpoint directory.
     */
    def getCheckpoint: String = { m_CheckpointDir }


    /**
     * Setup the <SparkStreaming> context and <Twitter> stream using parameters passed on the command line.
     * @return StreamingContext Spark Streaming Context.
     */
    def init(): StreamingContext =
    {
        // <<---- BEGIN: CONFIGURE APACHE SPARK STREAMING ---->>
        val sparkConf = new SparkConf()

        // Spark Context setup for local vs cluster.
        if (!sparkConf.contains("spark.master"))
        {
            sparkConf.setMaster("local[*]")
                .setAppName(k_Appname + " -- Local")
        }
        else { sparkConf.setAppName(k_Appname) }

        val streamingContext = new StreamingContext(sparkConf, k_BatchDuration)
        streamingContext.checkpoint(m_CheckpointDir)

        // <<---- END: CONFIGURE APACHE SPARK STREAMING ---->>


        // Setup <Twitter> stream.
        m_TweetStream = TwitterUtils.createStream(streamingContext, None)


        // Configure logger and display config.
        val log4j = Logger.getRootLogger
        log4j.info("Window Duration:   " + k_WindowDuration.toFormattedString)
        log4j.info("Slide Duration:    " + k_SlideDuration.toFormattedString)
        log4j.info("Batch Duration:    " + k_BatchDuration.toFormattedString)
        log4j.info("Threshold Level:   " + m_Threshold)
        log4j.info("Checkpoint Dir:    " + m_CheckpointDir)
        log4j.setLevel(Level.ERROR)


        //--------------------------------------------------------------------------------------------------------------
        var prevLocTotals: Double = 1
        val topLoc = extractLocation()

        topLoc.foreachRDD(rdd => {
            displayBanner()

            displayRDD("Locations", prevLocTotals, rdd.count)
            prevLocTotals = rdd.count()

            rdd.take(m_Threshold)
               .foreach{ case (item, count) => println("%s (%s tweets)".format(item, count)) }
        })


        var prevHandleTotals: Double= 1
        val topMention = extractHandles()

        topMention.foreachRDD(rdd => {
            displayRDD("Mentions", prevHandleTotals, rdd.count)
            prevHandleTotals = rdd.count()

            rdd.take(m_Threshold)
               .foreach{ case (item, count) => println("%s (%s tweets)".format(item, count)) }
        })


        var prevTagTotals: Double = 1
        val topTags = extractHashtags()

        topTags.foreachRDD(rdd => {
            displayRDD("Hash Tags", prevTagTotals, rdd.count)
            prevTagTotals = rdd.count()

            rdd.take(m_Threshold)
               .foreach{ case (item, count) => println("%s (%s tweets)".format(item, count)) }
        })

        //--------------------------------------------------------------------------------------------------------------


        streamingContext
    }


    /**
     * Check the <Twitter> stream status.
     * @return Boolean Whether the <Twitter> stream was setup. Does not check for connectivity.
     */
    def getTwitterStreamStatus: Boolean =
    {
        m_TweetStream != null
    }


    /**
     * Initiate processing of the stream.
     */
    def start(): Unit =
    {
        // Setup fault-tolerant streaming context.
        val streamingContext = StreamingContext.getOrCreate(m_CheckpointDir, init _)

        streamingContext.start()
        streamingContext.awaitTermination()
    }


    /**
     * Displays the frame banner on each iteration.
     */
    val displayBanner: () =>
        Unit = () =>
    {
        val banner: String = """|
                                |------------------------------------------------
                                |               TWITTER ANALYTICS
                                |Window Duration: %ssec   Slide Duration: %ssec
                                |------------------------------------------------
                                |"""
                                .stripMargin.format(k_WindowDuration.milliseconds / 1000,
                                                    k_SlideDuration.milliseconds / 1000)
        print(banner)
    }


    /**
     * Displays the processed RDD to standard out with count and trend information.
     * @param label Descriptive data label to be used in header.
     * @param previous Previous count used in delta calculation.
     * @param current Current count used in delta calculation.
     * @return Double Returns the delta between the previous and current RDD.
     */
    val displayRDD: (String, Double, Double) =>
        Double = (label: String, previous: Double, current: Double) =>
    {
        // Safety Check: Ensure that prior value passed in is not <0>.
        val prev: Double = if (previous == 0) 1d else previous

        print( "\nTop %s (%.0f total)".format(label, current) )

        val delta: Double = ((current - prev) / prev) * 100
        print(" (" )
        if (delta < 0) print(Console.RED + 0x25BC.toChar) else print(Console.GREEN + 0x25B2.toChar)
        println(" %.2f%%".format(delta) + Console.RESET + "):")

        delta
    }


    /**
     * Extracts the user location from the <Twitter> stream.
     * @return
     */
    def extractLocation(): DStream[(String, Int)] =
    {
        m_TweetStream.map(tweet => {if (tweet.getPlace != null) (tweet.getPlace.getCountry, 1) else null })
                     .filter(_ != null)
                     .reduceByKeyAndWindow( (x: Int, y: Int) => x + y, k_WindowDuration, k_SlideDuration)
                     .transform(_.sortBy(_._2, ascending=false))
    }


    /**
     * Extracts all the mentioned user handles from the <Twitter> stream.
     * @return
     */
    def extractHandles(): DStream[(String, Int)] =
    {
        // Regex rule based on https://help.twitter.com/en/managing-your-account/twitter-username-rules
        val handleRegex: Regex = "^@[_a-zA-Z0-9]{1,15}".r

        m_TweetStream.flatMap(_.getText.split("\\s+"))
                     .flatMap(handle => handleRegex.findFirstIn(handle).toList)
                     .map(handle => (handle.toLowerCase(), 1))
                     .reduceByKeyAndWindow( (x: Int, y: Int) => x + y, k_WindowDuration, k_SlideDuration)
                     .transform(_.sortBy(_._2, ascending=false))
    }


    /**
     * Extracts all the hashtags from the <Twitter> stream.
     */
    def extractHashtags(): DStream[(String, Int)] =
    {
        // Hashtags begin with a <#>, are alphanumeric, and of variable length.
        val hashtagRegex: Regex = "^#[A-Za-z0-9]+$".r

        m_TweetStream.flatMap(status => status.getText.split("\\s+"))
                     .flatMap(word => hashtagRegex.findFirstIn(word).toList)
                     .map(hashtag => (hashtag.toLowerCase(), 1))
                     .reduceByKeyAndWindow( (x: Int, y: Int) => x + y, k_WindowDuration, k_SlideDuration)
                     .transform(_.sortBy(_._2, ascending=false))
    }

}
