name := "Twitter-Analytics"

version := "1.0"

scalaVersion := "2.12.12"

libraryDependencies ++= Seq("org.apache.spark" %% "spark-streaming" % "3.0.1",
                            "org.apache.bahir" %% "spark-streaming-twitter" % "2.4.0",
                            "org.scalatest" %% "scalatest" % "3.2.0" % "test",
                            "com.holdenkarau" %% "spark-testing-base" % "3.0.1_1.0.0" % Test)

// Spark Testing Setup:
fork in Test := true
javaOptions ++= Seq("-Xms512M", "-Xmx2048M", "-XX:MaxPermSize=2048M", "-XX:+CMSClassUnloadingEnabled")
parallelExecution in Test := false

// Exclude <Driver.scala> from coverage report.
coverageExcludedPackages := "com.surahman.scala.twitter.analytics.Driver.*"

// Fat-Jar Setup:
assemblyJarName in assembly := "twitter-analytics.jar"
assemblyMergeStrategy in assembly :=
{
    case PathList("META-INF", xs @ _*) => MergeStrategy.discard
    case x => MergeStrategy.first
}
