package com.surahman.scala.twitter.analytics

            /*__________________________________________________#
            #  Life is a journey, and it's the experiences we   #
            #  have along the way that define us. To shy from   #
            #  the difficult path least traveled is a travesty, #
            #  for it's here that we find the greatest bounty.  #
            #                                                   #
            #               ~ Perpetual Form ~                  #
            #                                                   #
            #   For my father, who taught me the true meaning   #
            #                   of courage.                     #
            #___________________________________________________#
            # File Name    :    Driver.scala                    #
            # Dependencies :    LIB:                            #
            #                   USR:                            #
            # Toolchain    :    Scala 2.12.12, SBT 1.4.1,       #
            #                   Spark 3.0.1                     #
            # Compilation  :    sbt assembly                    #
            #___________________________________________________#
            #    https://www.linkedin.com/in/saad-ur-rahman/    #
            #  Copyright Saad Ur Rahman, All rights reserved.   #
            #     Usage and source code is subject to the       #
            #           GNU Affero GPL v3.0 license.            #
            #___________________________________________________#
            #                                                   #
            #             Twitter Analytics Driver              #
            #__________________________________________________*/




//- Import(s): Library:



            /*_________________________________________________ #
            #               Object Definitions                  #
            #__________________________________________________*/



object Driver {

    def main(args: Array[String]): Unit =
    {
        var twitterAnalytics: TwitterAnalytics = null

        try
        {
            twitterAnalytics = new TwitterAnalytics(args=args)
            twitterAnalytics.start()
        }
        catch
        {
            case error: Exception =>
            {
                System.err.println(error.getMessage)
                System.exit(-1)
            }
        }

    }

}
