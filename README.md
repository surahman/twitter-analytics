
<p align="center" width="100%">
    <img src="./img/tweet_256px.png">
</p>

## Twitter Analytics

[ License ] |
:---------: |

This project is made available under the `GNU Affero General Public License v3.0` for the sole purpose of demonstrating
my programming and software development capabilities. Please adhere to the permissions, conditions and limitations
which are outlined in the `GNU Affero General Public License v3.0`.

The license file is included as `LICENSE` in the root directory of this project and can also be found online
[here](https://opensource.org/licenses/AGPL-3.0 "GNU Affero General Public License v3.0")

<br>
<br>

[ System Specs ] |
:---------: |

This was the system configuration of the local development and test environments. Whilst it is recommended to have a
similar setup, newer versions of the tool sets should not break the build.

|Software| Version
|:------------: | :-------------:|
Java Runtime Environment | build 1.8.0_212-b10 x64
Scala | 2.12.12
Apache Spark | 3.0.1
SBT | 1.4.1
Windows 10 Pro | 2004 x64 (build 19041.610)

<br>
<br>

|[ Build ] |
|:---------: |

[![pipeline status](https://gitlab.com/surahman/twitter-analytics/badges/development/pipeline.svg)](https://gitlab.com/surahman/twitter-analytics/-/commits/development) [![coverage report](https://gitlab.com/surahman/twitter-analytics/badges/development/coverage.svg)](https://gitlab.com/surahman/twitter-analytics/-/commits/development)

- [x] Fat Jar: Running `sbt assembly` will assemble the `twitter-analytics.jar` fat jar file in the `target/scala-2.12` directory.
- [x] Tests: Running `sbt test` will execute the test suite within the project.

<br/>

__*Note on Testing:*__

`Spark Streaming` application testing presents a unique set of issues which arise from `serialization` and `checkpoint`ing. Since `Spark Streaming` applications are designed as persistent or long-running jobs, they must employ fault-tolerant recovery via checkpoints.

This means that all code which performs operations on the `RDD`s must be enclosed in the `closure`s for the `Transformation` and `Action`. Not doing so will raise a hard-failure on job submission. The context initialization routine is what is “shipped” to the `executors` to run. It should be noted that the majority of code indicated as uncovered in the coverage report is encased within `closure`s.

<br>
<br>

|[ Usage ] |
|:---------: |

The application takes **_5_** mandatory and **_1_** optional parameters in the following order:
- [x] Consumer Key: Your `Twitter` account consumer key. *_REQUIRED_*
- [x] Consumer Secret: Your `Twitter` account consumer secret. *_REQUIRED_*
- [x] Access Token: Your `Twitter` account access token. *_REQUIRED_*
- [x] Access Token Secret: Your `Twitter` account access token secret. *_REQUIRED_*
- [x] Checkpoint Directory: The directory where files will be checkpointed to recover from incase of failure. *_REQUIRED_*
- [x] Threshold: The number of items to display from each dimension.

You will need your own `Twitter` developer account to get the _Consumer_ and _Access_ keys and tokens.

###### __*Note:*__ _In a production environment where the application is deployed in the cloud, you would rely on a service like AWS’s Secrets Manager to store and retrieve the Twitter credentials. It is exceptionally insecure and an extremely poor policy to pass these keys and tokens as command line parameters. You could retrieve the credentials using the AWS Java API after you setup the security policies to allow access to the required services and stored secrets._

<br>

Examples:
```bash
twitter-analytics.jar consumer-key consumer-secret access-token access-token-secret checkpoint-directory [threshold]
```

Files can be located on a local hard disk, `HDFS`, or `S3`.


<br>
Spark Submit Examples:

```bash
# Run application locally on all cores
spark-submit \
  --class com.surahman.scala.twitter.analytics.Driver \
  --master local[*] \
  /path/to/twitter-analytics.jar \
  consumer-key consumer-secret access-token access-token-secret checkpoint-directory [threshold]

# Run on a YARN cluster
export HADOOP_CONF_DIR=XXX
spark-submit \
  --class com.surahman.scala.twitter.analytics.Driver \
  --master yarn \
  --deploy-mode cluster \  # can be client for client mode
  --executor-memory 5G \
  --num-executors 3 \
  /path/to/twitter-analytics.jar \
  consumer-key consumer-secret access-token access-token-secret checkpoint-directory [threshold]
```


<br>
<br>

|[ Objective ] |
|:---------:|
##### __Purpose__ ![Purpose](./img/tweet_32px.png)

It is exceptionally challenging to locate a free stream of data which can be ingested from a `Kafka` or `Flume` endpoint. __*Twitter*__ has been kind enough to provide developers with a free streaming endpoint for which open source connectors are abundantly available; much obliged __*Twitter, Inc*__.

The objective of this project was to play with the `Spark Streaming` framework, and in doing so to develop a deeper understanding of the limitations and pitfalls of development and testing. As outlined in the _Build_ section above, there are limitations to one’s ability to test the results of `Transformations` and `Actions` on a `DStream`. The `Spark Testing Base` has an immensely useful `StreamingSuiteBase` but employing it does not allow you to test the code encased within the `closures` in isolation. To test the `Transformations` and `Actions` on an `RDD` you would need to build mock `Twitter4j.status` objects and place them within an `RDD`. This mock `RDD` would then be utilized in the `StreamingSuiteBase` to simulate a live `DStream` whilst removing entropy and introducing predictability in the results.

The application I have written monitors a __*Twitter*__ stream looking at the last `60` seconds of tweets and displays the change in the last `10` seconds. The dimensions it is looking at are the Country, mentioned user handles, and hashtags in tweets. It displays a colour-coded arrow with the percentage change in the combined number of items in each dimension during the slide window period.

<br>
<br>

|[ Architecture ] |
|:---------:|

##### __Design__ ![Design](./img/tweet_24px.png)

###### _It is exceptionally straightforward to produce a monolithic Spark application with all the code in the driver. This will result in code which is impossible to unit test and is generally not reusable as library code._

<br>

The code is primarily designed for reuse through modularity, which also lends itself well to supporting unit testing. I will be testing the `RDD`s `Transformation` and `Action` pipeline using `Mockito` to mock the `Twitter4j.status` objects at a later time. The objective of testing is not to test the `Spark` framework but to test the `transformations` and `actions` being undertaken on the data are correct.

The constructor for `TwitterAnalytics` accepts optional arguments to set the `Window`, `Slide`, and `Batch` durations. It expects to be passed the `argument container` array gathered from the command line when the application boots. This `argument container` must contain the four `Twitter` authentication keys and tokens, as well as the location of where to collect `checkpointing` data. The `argument container` may optionally contain a `threshold` value used to acquire the prominent items from each dimension of the data. The default `threshold` value is set to `5`, `Window` duration to `60 seconds`, `Slide` duration to `10 seconds`, and `Batch` duration to `2 seconds`.

The `Spark Streaming Context` is setup for checkpoint based fault-tolerant recovery. To facilitate this, the driver application will attempt recovering a `Streaming Context` before it initiates a new one. To support this functionality, the `TwitterAnalytics` class contains an init member function which will setup the `Streaming Context` and __*Twitter*__ stream. All operations on the __*Twitter*__ stream are initiated from within the `init` function.

To support `checkpointing` the `Spark Streaming` framework imposes strict rules on `serializability` and what can be called from within a `closure`. To reduce the number of objects which have to be shipped to `executors` all operations executed within a `closure` must be defined within said `closure`. Failing to do so will result in a hard-failure by the framework on job dispatch. Non-serializability of functions called from within the `init` method is caught at both compile-time and the time of `Spark` job spooling.

<br>
<br>

|[ Output ] |
|:---------:|

##### __Sample__ ![Sample](./img/tweet_24px.png)

<br>

```text
------------------------------------------------
               TWITTER ANALYTICS
Window Duration: 60sec   Slide Duration: 10sec
------------------------------------------------

Top Locations (13.0 total) (▲ 44.44%):
United States (10 tweets)
Brasil (4 tweets)
México (2 tweets)
United Kingdom (2 tweets)
Argentina (2 tweets)

Top Mentions (2254.0 total) (▲ 0.04%):
@realdonaldtrump (29 tweets)
@bts_twt (12 tweets)
@rudygiuliani (9 tweets)
@kylekuzma (7 tweets)
@chelseaclinton (6 tweets)

Top Hash Tags (380.0 total) (▼ -2.56%):
#AMAs (15 tweets)
#ShopeexChanyeolEXO (13 tweets)
#WilsonElMejor (9 tweets)
#TeamJipyeong (9 tweets)
#teamhanjipyeong (8 tweets)
```

![Example Stream](https://surahman.gitlab.io/img/screenshots/twitter_streaming_analytics/live_example.gif)

<br>

##### __Insights__ ![Insights](./img/tweet_24px.png)

_For posterity's sake, this project was undertaken around the 2020 US election and its aftermath._

I am not a __*Twitter*__ consumer, so this project allowed me to observe how the platform is utilized and the structure of the tweet data. For the most part, this project allowed me to better understand the `serialization` limitations and how a streaming pipeline can be processed and tested.

When the job is initially spooled, it will display inaccurate `trend` metrics as the pipeline fills with data. Once the pipeline is full, which will typically be an integer factor of the `window` duration, the `trend` metrics will become accurate. The delta which is calculated takes into account all data looking back for the entirety of the `window` duration. The change is accounted for in the recent data collected during the `slide` duration.

It is fascinating to observe the number of commercial companies utilizing the platform as an advertising medium through the use of hashtags - I think it is a safe bet to assume these are all bots. If you watch the data at various times of the day, you can see the top Countries for tweets move around. During my testing, it was exceptionally rare to see the US get unseated from the top position. Something else I noted was that `#AMAs` (American Music Awards) seems to be trending with `@realdonaldtrump` (the _Real_ Donald Trump) being the most mentioned user handle. Go figure.


<br/>
<br/>


---
#### *__Development Log__*

---
<br/>

Projects are committed to a development repository where it is following the Git Flow paradigm.

<br/>


##### [ November 10th 2020 ]
> * Initial project workspace setup.
> * Setup `sbt` build script.
> * Began work on `bootstrap` parameter parsing to setup `StreamingContext`.

<br/>


##### [ November 11th 2020 ]
> * Changed `Utilities` from an `object` to a `class` with the name `TwitterAnalytics`.
> * Changed `bootstrap` routine to be called `init` and began work on `SparkStreaming` setup.
> * Setup and returning a `StreamingContext` as well as setting up an authenticated `Twitter` stream from `init` routine.

<br/>


##### [ November 12th 2020 ]
> * Removed redundant library dependancies from `sbt` file.
> * Began work on making `TwitterAnalytics` a class with a constructor to separate `Twitter` credential setup from initializing a `StreamingContext`.
> * Created constructor for `TwitterAnalytics` class, adjusted parameter handling,and fixed tests.
> * Added updated tests for `init`.
> * Completed and tested `Twitter Stream` setup and tested. Not tested for authorized connectivity.
> * Added startup code to `Driver`.
> * Created `start` routine to initialize streaming of data. `RDD` operations must be handled within the `init` routine as per the recommendations from [`Data Bricks` best practices](https://docs.databricks.com/spark/latest/rdd-streaming/developing-streaming-applications.html).
> * Fixed issues with non-serializability of the class. Issue arises as a requirement for the state needing to be written to storage for checkpoint recovery to make it fault-tolerant.

<br/>


##### [ November 13th 2020 ]
> * Basic Hash Tag scrapping completed and tested in `init`.
> * Added data collection for top `hash tag`, `mention`, and `country`.
> * Added place holder markers for `trend` which will update to show dynamic arrows with percentage.
> * Added colour coded trending arrows with percentage change. Take some time to warm up the cache before it becomes accurate.

<br/>


##### [ November 14th 2020 ]
> * Added header to output and cleaned up the display.
> * Tightened up code and with `val` function `displayRDD` and created tests.

<br/>


##### [ November 15th 2020 ]
> * Created routine to display info `banner` and tests.
> * Fixed bug with serializability in `displayRDD` after checkpoint recovery.
> * Isolated all operations on `RDD`s to enable mock testing using `Mockito` at a later stage.
> * Tightened up regex patterns for `hashtag` and `user handle` extraction.
> * Completing sections in the `Readme`.

<br/>


##### [ November 17th 2020 ]
> * Hot Fix: Lower case of all user handles.
> * Hot Fix: `extractHandles` and `extractHashtags` updated to use `flatMap` when running `regex` patterns. The captured pattern is returned in a `list` and all `nil` (not matched) items are discarded by `flatMap`.

<br/>

---

###### Copyright and Legal Notice(s):
&copy; Saad Ur Rahman. All rights reserved. Usage and source code is licensed under the
`GNU Affero General Public License v3.0`.

Icons made by [Freepik](https://www.freepik.com/ "Freepik") from
[www.flaticon.com](https://www.flaticon.com/ "Flaticon")
is licensed by [CC 3.0 BY](http://creativecommons.org/licenses/by/3.0/ "Creative Commons BY 3.0").

---
