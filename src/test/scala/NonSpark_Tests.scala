package com.surahman.scala.twitter.analytics

import org.apache.spark.streaming.Seconds
import org.scalatest.funsuite.AnyFunSuite

class NonSpark_Tests
    extends AnyFunSuite
{

    test("<init> throws illegal argument error on incorrect parameter count.")
    {
        val args = Array("consumer-key", "consumer-secret", "access-token", "access-token-secret",
                         "checkptDir", "threshold", "poo-poo")

        assertThrows[IllegalArgumentException]
        { val testApp = new TwitterAnalytics(args=Array()) }

        assertThrows[IllegalArgumentException]
        { val testApp = new TwitterAnalytics(args=args.slice(0, 1)) }

        assertThrows[IllegalArgumentException]
        { val testApp = new TwitterAnalytics(args=args.slice(0, 2)) }

        assertThrows[IllegalArgumentException]
        { val testApp = new TwitterAnalytics(args=args.slice(0, 3)) }

        assertThrows[IllegalArgumentException]
        { val testApp = new TwitterAnalytics(args=args) }


        // Ensure parameters pass.
        try
        { val testApp = new TwitterAnalytics(args=args.slice(0, 5)) }
        catch { case error: Exception => fail("<bootstrap> failed when correctly provided all parameters.") }

        try
        { val testApp = new TwitterAnalytics(args=args.slice(0, 6)) }
        catch { case error: Exception => fail("<bootstrap> failed when correctly provided all parameters but threshold.") }

    }

    test("<init> correctly sets up default data members.")
    {
        val args = Array("consumer-key", "consumer-secret", "access-token", "access-token-secret")
        val testApp = new TwitterAnalytics(args=args)

        assertResult(Seconds(60), "Incorrectly set default window duration.")
        { testApp.k_WindowDuration }

        assertResult(Seconds(10), "Incorrectly set default slide duration.")
        { testApp.k_SlideDuration }

        assertResult(Seconds(2), "Incorrectly set batch duration.")
        { testApp.k_BatchDuration }

        assertResult(5, "Incorrectly set default threshold.")
        { testApp.getThreshold }

        assertResult("twitter_analytics_checkpt", "Incorrectly set default checkpoint directory.")
        { testApp.getCheckpoint }
    }

    test("<init> correctly sets up non-default data members.")
    {
        val args = Array("consumer-key", "consumer-secret", "access-token", "access-token-secret",
                         "some checkpoint dir", "7")
        val testApp = new TwitterAnalytics(k_WindowDuration=Seconds(90),
                                           k_SlideDuration=Seconds(30),
                                           k_BatchDuration=Seconds(5),
                                           args=args)

        assertResult(Seconds(90), "Incorrectly set window duration.")
        { testApp.k_WindowDuration }

        assertResult(Seconds(30), "Incorrectly set slide duration.")
        { testApp.k_SlideDuration }

        assertResult(Seconds(5), "Incorrectly set batch duration.")
        { testApp.k_BatchDuration }

        assertResult(7, "Incorrectly set threshold.")
        { testApp.getThreshold }

        assertResult("some checkpoint dir", "Incorrectly set checkpoint directory.")
        { testApp.getCheckpoint }
    }
}
